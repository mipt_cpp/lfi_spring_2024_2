# lfi_spring_2024_2

---
title: ЛФИ Б02-312
---

# Конспекты

## Часть №1

1. [Введение. Основы синтаксиса № 1](./notes/intro_1.md)
2. [Введение. Основы синтаксиса № 2](./notes/intro_2.md)
3. [Массивы](./notes/arrays.md)
4. [Динамическая память](./notes/memory.md)
5. [Сортировки](./notes/sorts.md)
6. [Структуры](./notes/structs.md)
7. [Структуры данных](./notes/seq_data_structs.md)
8. [Бинарные деревья](./notes/bin_trees.md)

# Контесты

1. [Тренировочный](https://contest.yandex.ru/contest/59310/enter) - бессрочно
2. [Рекурсия](https://contest.yandex.ru/contest/59603/enter)

# Таблица посещаемости и задач

[Таблица](https://docs.google.com/spreadsheets/d/1CGOFCojs4aSufLh8KIu3gD0ERtGCzmPzkEU-U0jZR38/edit?usp=sharing)

# Основные ссылки/ресурсы для поиска ответов на вопросы

1. Документация по языку + примеры - https://en.cppreference.com/w/
2. Ответы на вопросы - https://stackoverflow.com/
3. Онлайн компилятор для быстрого просмотра и запуска кода - https://godbolt.org/
4. CMake документация - https://cmake.org/
5. Компилятор gcc - https://gcc.gnu.org/
6. Компилятор clang - https://clang.llvm.org/
7. Консоль для Windows с возможностью установить CMake и другие нужные утилиты - https://www.msys2.org/
8. Удобная IDE - https://www.jetbrains.com/clion/
9. Удобный редактор кода - [https://code.visualstudio.com/](https://www.vim.org/)
10. Гайд по отладке в VS Code - https://code.visualstudio.com/docs/cpp/cpp-debug
11. Как надо писать код на плюсах - https://github.com/isocpp/CppCoreGuidelines
12. Чуть больше, чем просто просмотр кода - https://cppinsights.io/
